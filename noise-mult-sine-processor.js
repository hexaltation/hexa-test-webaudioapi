// noise-mult-sine-processor.js
class NoiseMultSineProcessor extends AudioWorkletProcessor {
    process (inputs, outputs, parameters) {
      const output = outputs[0]
      const frequency = parameters.frequency || 400;
      const delta_t = 1/sampleRate;
      let time = 0;
      output.forEach(channel => {
        for (let i = 0; i < channel.length; i++) {
          let noise = Math.random() * 2 - 1
          let sine = Math.sin(2 * Math.PI * frequency * time);
          channel[i] = noise * sine
          time += delta_t;
        }
      })
      return true
    }
}
  
registerProcessor('noise-mult-sine-processor', NoiseMultSineProcessor)
