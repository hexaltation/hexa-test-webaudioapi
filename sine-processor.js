// sine-processor.js
class SineProcessor extends AudioWorkletProcessor {
    process (inputs, outputs, parameters) {
      const output = outputs[0]
      const frequency = parameters.frequency || 400;
      const delta_t = 1/sampleRate;
      let time = 0;
      output.forEach(channel => {
        for (let i = 0; i < channel.length; i++) {
          channel[i] = Math.sin(2 * Math.PI * frequency * time);
          time += delta_t;
        }
      })
      return true
    }
}
  
registerProcessor('sine-processor', SineProcessor)
