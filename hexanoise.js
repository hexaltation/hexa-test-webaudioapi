var audioContext;

let hexaNoise = async()=>{
    createAudioWorklet("white-noise-processor");
}

let hexaSine = async()=>{
    createAudioWorklet("sine-processor");
}

let hexaNoisePlusSine = async()=>{
    createAudioWorklet("noise-plus-sine-processor");
}

let hexaNoiseMultSine = async()=>{
    createAudioWorklet("noise-mult-sine-processor");
}

let hexaNoiseMinusSine = async()=>{
    createAudioWorklet("noise-minus-sine-processor");
}

let createAudioWorklet = async(name)=>{
    if (audioContext){
        audioContext.close();
        delete audioContext;
    }
    audioContext = new AudioContext();
    await audioContext.audioWorklet.addModule(`${name}.js`);
    const audioNode = new AudioWorkletNode(audioContext, name);
    audioNode.connect(audioContext.destination);
}