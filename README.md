# Hexanoise
Experimentations of white noise modulation with WebAudioAPI

## Run
Run in the project directory
``` shell
$ python3 -m http.server
```
Page will be served at `localhost:8000`

## ALERT
**At 25th of march 2020 only up-to-date Chrome seems to handle AudioWorkletObject of WebAudioAPI so please test this code with it.**